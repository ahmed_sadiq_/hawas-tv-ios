//
//  NetworkManager.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
typedef void (^loadSuccess)(id data);
typedef void (^loadFailure)(NSError *error);

+(void) postURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  getURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  putURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;



+(void) postURIWithFormDataArray:(NSArray *) formdataArray
                      parameters:(NSDictionary *) params
                             url:(NSString *)uri
                         success:(loadSuccess) success
                         failure:(loadFailure) failure;



@end
