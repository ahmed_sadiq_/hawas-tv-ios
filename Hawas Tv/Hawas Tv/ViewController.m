//
//  ViewController.m
//  Hawas Tv
//
//  Created by Samreen on 17/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "ViewController.h"
#import "PostService.h"
#import "HomeHeaderView.h"
#import "HomeCell.h"
#import "UIImageView+URL.h"
#import "Programs.h"
#import "ProgramDetailVC.h"
#import "Reachability.h"


@interface ViewController ()<ProgramHeaderViewDelegate>
{
    NSMutableArray *programsAarray;
    int pageNo;
    BOOL dataArrayISEmpty;
    BOOL serverCall;
}
@end

@implementation ViewController


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"detail" ]) {
        ProgramDetailVC  *progVC = segue.destinationViewController;
        progVC.selectProgram = sender;
        
    }
}

-(void)didPressProgramDetailedButton:(NSInteger )tag{
    NSLog(@"tag is :%ld",tag);
    Programs *prog = programsAarray[tag];
    [self performSegueWithIdentifier:@"detail" sender:prog];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    programsAarray = [[NSMutableArray alloc]init];
    programsAarray = [DataManager sharedManager].programArray.mutableCopy;
    [self setupRefreshControl];
    pageNo = 1;
    dataArrayISEmpty =NO;
    serverCall = NO;
    [self getPrograms];
    
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - Table View Methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *HeaderCellIdentifier = @"header";
    HomeHeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    if (headerView == nil) {
        headerView = [[HomeHeaderView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    headerView.headerDelegate = self;
//    headerView.isOverlay = YES;
//    headerView.isFeatured = NO;
//    headerView.isLatesNews = NO;
    headerView.isShadowOverlay = NO;
    headerView.isFeatured = YES;
    headerView.isLatesNews = NO;
    NSArray *featuredArray = [DataManager sharedManager].featuredProgram;
    [headerView setUpScrollView:featuredArray.mutableCopy];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  
        return 294;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
        return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSArray *featuredArray = [DataManager sharedManager].featuredProgram;
    for (int i = 0; i <featuredArray.count; i++) {
        FeaturedPrograms *fProg = featuredArray[i];
        for (int j = 0; j<programsAarray.count; j++) {
            Programs *prog = programsAarray[j];
            if(fProg.programId == prog.programId){
                [programsAarray removeObjectAtIndex:j];
            }
        }
    }
    
    return programsAarray.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Programs *prog = programsAarray[indexPath.row];
    HomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell==nil) {
        
        cell =[[HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    [cell.postImg setImagewithURL: prog.programImg];
    cell.lblTitle.text = prog.programTitle;
    NSString * timeStr = [prog.programTiming  componentsJoinedByString:@","];
    cell.lblCategory.text = timeStr;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Programs *prog = programsAarray[indexPath.row];

    [self performSegueWithIdentifier:@"detail" sender:prog];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
        NSArray *visibleRows = [_tblView visibleCells];
        UITableViewCell *lastVisibleCell = [visibleRows lastObject];
        NSIndexPath *path = [_tblView indexPathForCell:lastVisibleCell];
        NSLog(@"%ld",(long)path.row);
        if(path.section == 0 && path.row == (programsAarray.count)-1)
        {
            if ( !serverCall && !dataArrayISEmpty) {
                    pageNo++;
                    [self getPrograms];
            }
        }
}

-(void) getPrograms{
        serverCall = YES;
        NSString *page = [NSString stringWithFormat:@"%d", pageNo];
        [PostService getProgramsWithPageNumber:page PageSize:@"10" success:^(id data) {
            [self.refreshControl endRefreshing];
            _tblView.hidden = NO;
            NSArray *tempPost = data;
            if (tempPost.count == 0) {
                dataArrayISEmpty = YES;
            }
            if (pageNo==1) {
                [programsAarray removeAllObjects];
                
                programsAarray = data;
                [_tblView reloadData];
                serverCall = NO;
                dataArrayISEmpty = NO;
            }
            else{
                _count = programsAarray.count;
                for (Programs *place in tempPost) {
                    [programsAarray addObject:place];
                }
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                int startIndex = (pageNo-1) *10;
                for (NSInteger i = _count ; i < _count+10; i++) {
                    if(i<programsAarray.count) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                }
                [_tblView beginUpdates];
                [_tblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                [_tblView endUpdates];
                serverCall = NO;
                
            }
            NSLog(@"%@",data);
        } failure:^(NSError *error) {
            NSLog(@"%@",error.localizedDescription);
            
        }];
    
}

#pragma mark - InternetConnection Method

- (BOOL)internetAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"إعادة المحاولة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self getPrograms];
    }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}

- (void)refreshHome:(id)sender
{
    pageNo=1;
    if (!serverCall) {
        [self getPrograms];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
