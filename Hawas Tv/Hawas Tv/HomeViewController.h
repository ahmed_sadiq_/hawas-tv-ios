//
//  HomeViewController.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "HomeFooterView.h"

@interface HomeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,iCarouselDataSource, iCarouselDelegate, HomeFooterViewDelegate>

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) HomeFooterView *footerView;
@property (strong, nonatomic) NSArray *latestNewsArray;
@property (strong, nonatomic) NSArray *featuredArray;

@property (assign, nonatomic) NSInteger firstTimeCount;
@property (assign, nonatomic) NSInteger count;
@property (assign, nonatomic) NSInteger progCount;
@property (assign, nonatomic) int check;


@end
