//
//  OptionsCell.h
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOption;
@property (weak, nonatomic) IBOutlet UIImageView *optImg;

@end
