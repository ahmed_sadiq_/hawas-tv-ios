//
//  SigninViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "SigninViewController.h"
#import "UITextField+Style.h"
#import "NetworkManager.h"
#import "Constant.h"
#import "SettingVc.h"
#import <GONMarkup.h>
#import <GONMarkupParserManager.h>
#import <GONMarkupNamedColor.h>
#import <SVProgressHUD.h>


@interface SigninViewController ()<UITextFieldDelegate>

@end

@implementation SigninViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self initialization];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialization{
    [_tfEmail setRightSpaceSize:10];
    [_tfPassword setRightSpaceSize:10];
    
    NSString *inputText = @"<white>ليس لديك حساب؟</> <yellow>إنضم الأن</>";
    [[GONMarkupParserManager sharedParser] addMarkup:[GONMarkupNamedColor namedColorMarkup:[UIColor colorWithRed:255.0f/255.0f green:191.0f/255.0f blue:163.0f/255.0f alpha:1.0f]
                                                                                    forTag:@"yellow"]];
    [[GONMarkupParserManager sharedParser] addMarkup:[GONMarkupNamedColor namedColorMarkup:[UIColor whiteColor]
                                                                                    forTag:@"white"]];
    [_btnSignup setAttributedTitle:[[GONMarkupParserManager sharedParser] attributedStringFromString:inputText
                                                                                               error:nil] forState:UIControlStateNormal];
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

-(void)dismissKeyboard {
    [_tfEmail resignFirstResponder];
    [_tfPassword resignFirstResponder];
}

#pragma mark - Btn Delegate

- (IBAction)btnforgotPressed:(UIButton *)sender {
}

- (IBAction)btnBackPressed:(UIButton *)sender {
//    SettingVc *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVc"];
//    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSigninPressed:(UIButton *)sender {
    if( [_tfEmail.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال البريد الإلكتروني"];
    }
    else if([_tfPassword.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال كلمة السر"];
    }
    else if(![self validateEmailWithString:_tfEmail.text]){
        [self showAlert:@"تنسيق البريد الإلكتروني غير صحيح"];
    }
    else{
        NSDictionary * postParam = @{
                                     @"request":URI_SIGN_IN,
                                     @"email":_tfEmail.text,
                                     @"password":_tfPassword.text
                                     };
        [self makeNetworkCallWithParam:postParam];
    }
}

#pragma mark - Network Call
-(void) makeNetworkCallWithParam:(NSDictionary *) postParam{
    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        [SVProgressHUD dismiss];
        NSDictionary *responseObj = data[@"user_data"];
        NSString *message = [responseObj objectForKey:@"message"];
        
        NSLog(@"sign in %@",responseObj);
        NSString *email = [responseObj objectForKey:CONSTANT_EMAIL];
        NSString *name = [responseObj objectForKey:CONSTANT_NAME];
        NSString *fName = [responseObj objectForKey:CONSTANT_FAMILY_NAME];
        NSString *userID = [responseObj objectForKey:CONSTANT_USER_ID];
        
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:CONSTANT_EMAIL];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:CONSTANT_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:fName forKey:CONSTANT_FAMILY_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:CONSTANT_USER_ID];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSignin"];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        
        
    } failure:^(NSError *error) {
        NSLog(@"sign in error %@",error.localizedDescription);
        [self showAlert:error.localizedDescription];
    }];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"alert");
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
