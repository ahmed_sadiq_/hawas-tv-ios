//
//  CategoryCell.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 06/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryCell;
@protocol CategoryCellDelegate <NSObject>

-(void)didTapCategoryWithIndex:(NSInteger) selectedIndex;

@end

@interface CategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) NSArray *dataArray;
@property (strong, nonatomic) id<CategoryCellDelegate> cellDelegate;

-(void) setUpCategory:(NSArray *)nameArray withIndex:(NSInteger) index withWidth:(CGFloat)width;
@end
