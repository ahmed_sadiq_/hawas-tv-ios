//
//  Competition.m
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "Competition.h"

@implementation Competition
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.competition_title = [self validStringForObject:responseData[@"competition_title"]];
        self.competition_id = [self validStringForObject:responseData[@"competition_id"]];
        self.competition_description = [self validStringForObject:responseData[@"competition_description"]];
        self.competition_image = [self validStringForObject:responseData[@"competition_image"]];
        self.competition_answers = responseData[@"competition_answers"];
        
        
    }
    
    return self;
}

@end
