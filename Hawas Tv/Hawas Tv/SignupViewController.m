//
//  SignupViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "SignupViewController.h"
#import "UITextField+Style.h"
#import "NetworkManager.h"
#import "Constant.h"
#import "SettingVc.h"
#import <GONMarkup.h>
#import <GONMarkupParserManager.h>
#import <GONMarkupNamedColor.h>
#import <SVProgressHUD.h>

@interface SignupViewController ()<UITextFieldDelegate>

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self initialization];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)initialization{
    [_tfName setRightSpaceSize:10];
    [_tfFname setRightSpaceSize:10];
    [_tfEmail setRightSpaceSize:10];
    [_tfPassword setRightSpaceSize:10];
    [_tfConfirmPswd setRightSpaceSize:10];
    
    NSString *inputText = @"<white>لديك حساب؟</> <yellow>تسجيل دخول</>";
    [[GONMarkupParserManager sharedParser] addMarkup:[GONMarkupNamedColor namedColorMarkup:[UIColor colorWithRed:255.0f/255.0f green:191.0f/255.0f blue:163.0f/255.0f alpha:1.0f]
                                                                                    forTag:@"yellow"]];
    [[GONMarkupParserManager sharedParser] addMarkup:[GONMarkupNamedColor namedColorMarkup:[UIColor whiteColor]
                                                                                    forTag:@"white"]];
    [_btnSignin setAttributedTitle:[[GONMarkupParserManager sharedParser] attributedStringFromString:inputText
                                                                                               error:nil] forState:UIControlStateNormal];

}

-(void)dismissKeyboard {
    [_tfName resignFirstResponder];
    [_tfFname resignFirstResponder];
    [_tfEmail resignFirstResponder];
    [_tfPassword resignFirstResponder];
    [_tfConfirmPswd resignFirstResponder];
    
}


- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - TextField Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark - Btn Delegate

- (IBAction)btnSigninPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSignupPressed:(UIButton *)sender {
    NSCharacterSet *familySet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
    
    NSCharacterSet * nameSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "] invertedSet];
    
    if([_tfName.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال الاسم"];
    }
    else if ([_tfName.text rangeOfCharacterFromSet:nameSet].location != NSNotFound) {
        [self showAlert:@"تنسيق الاسم غير صحيح"];
    }
    else if([_tfFname.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال الشهرة"];
    }
    else if ([_tfFname.text rangeOfCharacterFromSet:familySet].location != NSNotFound) {
        [self showAlert:@"تنسيق الشهرة غير صحيح"];
    }
    else if([_tfEmail.text isEqualToString:@""] ){
        [self showAlert:@"الرجاء إدخال البريد الإلكتروني"];
    }
    else if([_tfPassword.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال كلمة السر"];
    }
    else if([_tfConfirmPswd.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال تأكيد كلمة السر"];
    }
    else if(![_tfConfirmPswd.text isEqualToString:_tfPassword.text]){
        [self showAlert:@"كلمة المرور وتأكيد كلمة المرور غير متطابقين"];
    }
    else if(![self validateEmailWithString:_tfEmail.text]){
         [self showAlert:@"تنسيق البريد الإلكتروني غير صحيح"];
    }else{
    
        NSDictionary *postParam = @{@"request":URI_SIGN_UP,
                                    CONSTANT_EMAIL:_tfEmail.text,
                                    CONSTANT_NAME:_tfName.text,
                                    CONSTANT_FAMILY_NAME:_tfFname.text,
                                    @"password":_tfPassword.text};
        [self makeServerCallWithParams:postParam];
    }
}



#pragma mark - Network Call

- (void) makeServerCallWithParams:(NSDictionary *)postParam{

    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        [SVProgressHUD dismiss];
        NSDictionary *responseObj = data[@"user_data"];
        NSString *message = [responseObj objectForKey:@"message"];
        NSLog(@"sign up: %@",responseObj);

        NSString *email = [responseObj objectForKey:CONSTANT_EMAIL];
        NSString *name = [responseObj objectForKey:CONSTANT_NAME];
        NSString *fName = [responseObj objectForKey:CONSTANT_FAMILY_NAME];
        NSString *userID = [responseObj objectForKey:CONSTANT_USER_ID];
        
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:CONSTANT_EMAIL];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:CONSTANT_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:fName forKey:CONSTANT_FAMILY_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:CONSTANT_USER_ID];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSignin"];
        
        SettingVc *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVc"];
        [self.navigationController pushViewController:vc animated:YES];
        
            } failure:^(NSError *error) {
                NSLog(@"Failure: %@",error.localizedDescription);
                [self showAlert:error.localizedDescription];
    }];
}

#pragma mark - Alert Method

-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"alert");
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}


@end
