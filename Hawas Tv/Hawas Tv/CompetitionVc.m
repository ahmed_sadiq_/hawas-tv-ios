//
//  CompetitionVc.m
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "CompetitionVc.h"
#import "UIImageView+URL.h"
#import "JoinCompetitionVC.h"
#import "PostService.h"

@interface CompetitionVc ()
{
    Competition *competition;
}
@end

@implementation CompetitionVc



-(void) setUpView{
    _lblTitle.text = competition.competition_title;
    [_compPhoto setImagewithURL:competition.competition_image];
//    _lblGiftName.text = competition.competition_description;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCompetition];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    competition = [DataManager sharedManager].competition;
    [self setUpView];
}
- (IBAction)btnJoin:(id)sender {
    [self performSegueWithIdentifier:@"join" sender:nil];

}
-(void)getCompetition{
    [PostService getCompetitionSuccess:^(id data) {
        competition = [DataManager sharedManager].competition;
        [self setUpView];
    } failure:^(NSError *error) {
        
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
