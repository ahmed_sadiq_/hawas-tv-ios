//
//  HomeHeaderView.m
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "HomeHeaderView.h"
#import "UIImageView+URL.h"
#import "Programs.h"
#import "FeaturedPrograms.h"


@implementation HomeHeaderView
@synthesize headerDelegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageScroller.delegate = self;
    _pageControll.currentPage =0;
    // Initialization code
}

-(void) setUpScrollView:(NSArray *)programsArray{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat scrollWidth = screenRect.size.width;
    CGFloat scrollHeight = 294;
    
    for (UIView *v in [_imageScroller subviews]) {
        [v removeFromSuperview];
    }
    
    
    int xOffset = 0;
    
    
    for(int index=0; index < [programsArray count]; index++)
    {
        
        if(_isFeatured){
            _fProg = programsArray [index];
        }
        else if(_isLatesNews){
            _latestNews = programsArray[index];
        }
        else{
            _prog = programsArray [index];
        }
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,0,scrollWidth, scrollHeight)];
        [img setContentMode:UIViewContentModeScaleAspectFill];
        img.clipsToBounds = YES;
        
        if(_isFeatured){
            [img setImagewithURL:_fProg.programImg];
        }else if(_isLatesNews){
             [img setImagewithURL:_latestNews.newsImage];
        }
        else{
            [img setImagewithURL:_prog.programImg];
        }
        UIImageView *imgOverLay;
       imgOverLay = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,0,scrollWidth, scrollHeight)];
        imgOverLay.backgroundColor = [[UIColor colorWithRed:152.0/255.0 green:48.0/255.0 blue:220.0/255.0 alpha:1.0] colorWithAlphaComponent:0.7];
        
        if(_isShadowOverlay){
            imgOverLay = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,scrollHeight/2,scrollWidth, scrollHeight)];
            imgOverLay.backgroundColor = [UIColor clearColor];
            CAGradientLayer *layer = [CAGradientLayer layer];
            layer.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor];
            layer.frame = CGRectMake(0.0f, 0.0f, scrollWidth, scrollHeight/2);
            [imgOverLay.layer addSublayer:layer];
        }
        [_imageScroller addSubview:img];
        [_imageScroller addSubview:imgOverLay];
        
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(scrollWidth * index,scrollHeight/2 - 15,scrollWidth, 30)];
        
        if(_isFeatured){
            title.text = _fProg.programTitle;
        }else if(_isLatesNews){
            title.text = _latestNews.newsCatName;
        }
        else{
            title.text = _prog.programTitle;
        }
        
        title.font = [UIFont fontWithName:@"Cairo-Bold" size:25];
        title.textColor = [UIColor colorWithRed:250.0/255.0 green:183.0/255.0 blue:86.0/255.0 alpha:1.0];
        title.textAlignment = NSTextAlignmentCenter;
        [_imageScroller addSubview:title];
        
        UILabel *descriptionLbl = [[UILabel alloc] initWithFrame:CGRectMake(scrollWidth * index,title.frame.origin.y + 30,scrollWidth, 70)];
        
        if(_isFeatured){
            NSString * timeStr = [_fProg.programTiming  componentsJoinedByString:@","];
            descriptionLbl.text = timeStr;
            descriptionLbl.textColor = [UIColor colorWithRed:250.0/255.0 green:183.0/255.0 blue:86.0/255.0 alpha:1.0];
        }else if(_isLatesNews){
            descriptionLbl.text = _latestNews.newsTitle;
            descriptionLbl.textColor = [UIColor whiteColor];
        }
        else{
            descriptionLbl.text = _prog.programDescription;
            descriptionLbl.textColor = [UIColor colorWithRed:250.0/255.0 green:183.0/255.0 blue:86.0/255.0 alpha:1.0];
        }
        
        descriptionLbl.font = [UIFont fontWithName:@"Cairo-SemiBold" size:18];
        descriptionLbl.lineBreakMode = NSLineBreakByWordWrapping;
        descriptionLbl.numberOfLines = 2;
        descriptionLbl.textAlignment = NSTextAlignmentCenter;
        [_imageScroller addSubview:descriptionLbl];
        
        UIButton *programBtn = [[UIButton alloc] initWithFrame:CGRectMake(xOffset,0,scrollWidth, scrollHeight)];
        programBtn.backgroundColor = [UIColor clearColor];
        [programBtn addTarget:self action:@selector(programButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        programBtn.tag = index;
        [_imageScroller addSubview:programBtn];
        
        
        xOffset+=scrollWidth;
    }
    
    _imageScroller.contentSize = CGSizeMake(xOffset,scrollHeight);
    _imageScroller.pagingEnabled = YES;
    _imageScroller.bounces = NO;
    _pageControll.numberOfPages = programsArray.count;
    if (programsArray.count == 0) {
        _pageControll.hidden =YES;
    }
    
}

-(void)programButtonTapped:(UIButton *)sender{
    NSLog(@"COOL!");
        [self.headerDelegate didPressProgramDetailedButton:sender.tag];
    
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.pageControll.currentPage = page;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



@end
