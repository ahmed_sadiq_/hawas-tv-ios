//
//  Competition.h
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "BaseEntity.h"

@interface Competition : BaseEntity
@property (strong, nonatomic) NSString *competition_title;
@property (strong, nonatomic) NSString *competition_id;
@property (strong, nonatomic) NSString *competition_description;
@property (strong, nonatomic) NSString *competition_image;
@property (strong, nonatomic) NSArray *competition_answers;
- (id)initWithDictionary:(NSDictionary *) responseData;
@end
