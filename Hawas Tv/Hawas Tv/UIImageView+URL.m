//
//  UIImageView+URL.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "UIImageView+URL.h"
#import "UIImageView+WebCache.h"

@implementation UIImageView (URL)

- (void) setImagewithURL:(NSString *) strURL
{
    [self sd_setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"loading"]];
}
- (void) setSignImageWithURL:(NSString *) strURL
{
    [self sd_setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"no-sig"]];
}

@end
