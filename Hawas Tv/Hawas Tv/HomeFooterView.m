//
//  HomeFooterView.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "HomeFooterView.h"
#import "Programs.h"
#import "UIImageView+URL.h"
#import "FeaturedPrograms.h"
#import "DataManager.h"

@implementation HomeFooterView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) setUpScrollView:(NSArray *)programsArray{
    _progArray = programsArray;
    [_carousel reloadData];
}

//-(void)programButtonTapped:(UIButton *)sender{
//    NSLog(@"Footer view tapped %ld",(long)sender.tag);
//    
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    _featuredArray = [DataManager sharedManager].featuredProgram;
    for (int i = 0; i <_featuredArray.count; i++) {
        FeaturedPrograms *fProg = _featuredArray[i];
        for (int j = 0; j<_progArray.count; j++) {
            Programs *prog = _progArray[j];
            if(fProg.programId == prog.programId){
                [_progArray removeObjectAtIndex:j];
            }
        }
    }
    return _progArray.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{

    if(_check){
    _check = NO;
    _carousel.currentItemIndex = ([_progArray count]-1+_featuredArray.count) - (10 * _index);
        
    }
    if(_indexCheck){
        _carousel.currentItemIndex = _index;
        _indexCheck = false;
    }

    Programs *prog = _progArray [index];
    UILabel *title = nil;
    UIImageView *img = nil;
    UILabel *programDescription = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        view = [[UIView alloc] initWithFrame:CGRectMake(_carousel.frame.origin.x+50,0,_carousel.frame.size.width-100, _carousel.frame.size.height)];
        img = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,view.frame.size.width, view.frame.size.height-70)];
        [img setContentMode:UIViewContentModeScaleToFill];
        img.clipsToBounds = NO;
        img.tag = 1;
        [view addSubview:img];
        
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(0, img.frame.origin.y + img.frame.size.height + 5, view.frame.size.width, 22)];
        title.font = [UIFont fontWithName:@"Cairo-Bold" size:16];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.tag = 2;
        [view addSubview:title];
        
        
        programDescription = [[UILabel alloc] initWithFrame:CGRectMake(0,title.frame.size.height + title.frame.origin.y ,view.frame.size.width, 60)];
        programDescription.lineBreakMode = NSLineBreakByWordWrapping;
        programDescription.numberOfLines = 2;
        programDescription.font = [UIFont fontWithName:@"Cairo-Bold" size:13];
        programDescription.textColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.5];
        programDescription.textAlignment = NSTextAlignmentCenter;
        programDescription.tag = 3;
        [view addSubview:programDescription];
    }
    else
    {
        //get a reference to the label in the recycled view
        img = (UIImageView *)[view viewWithTag:1];
        title = (UILabel *)[view viewWithTag:2];
        programDescription = (UILabel *)[view viewWithTag:3];
       // programBtn = (UIButton *)[view viewWithTag:4];
    }
    
    
    [img setImagewithURL:prog.programImg];
    
    title.text = prog.programTitle;
    NSString * timeStr = [prog.programTiming  componentsJoinedByString:@","];
    programDescription.text = timeStr;
    
    return view;
}

-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    [self.cellDelegate didPressFooterProgramButton:index];
}
-(void)carouselDidEndDecelerating:(iCarousel *)carousel{
    if(carousel.currentItemIndex == _progArray.count - 1)
    {
        //[self.cellDelegate didEndCarousel];
    }
    if(carousel.currentItemIndex == 0)
    {
        [self.cellDelegate didEndCarousel];
    }
}
-(void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate{
    
}

-(void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
    NSLog(@"HomeFooter index %ld",carousel.currentItemIndex);
    
    [self.cellDelegate currentIndexRow:carousel.currentItemIndex];
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.1;
    }
    return value;
}
@end
