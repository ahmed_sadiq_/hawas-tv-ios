//
//  Constant.h
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)
/*  ------------------- BASE URL ------------------- */

#if DEBUG
#define BASE_URL            @"http://hawastv.com/api/request.php"   // Stagging
#else
#define BASE_URL            @"http://hawastv.com/api/request.php"  // Production

#endif

/* BASE URL END */

/*  ------------------ LOGIN ------------------ */

#define URI_SIGN_UP             @"userSignup"
#define URI_SIGN_IN             @"userLogin"
#define URI_FORGET_PASSWORD     @"forgotPassword"
#define URL_EDIT_PROFILE        @"editUserProfile"
#define URL_SETTINGS            @""
#define URL_SAVE_POST           @""
#define URL_DELETE_POST          @""
#define URL_UPDATE_REVIEWS       @""

#define URL_GET_PROGRAMS           @"getPrograms"
#define URL_GET_NEWS           @"getNews"
#define URL_GET_EPISODES           @"getEpisodes"
#define URL_SUBMIT_COMPETITION           @"submitCompetition"
#define URL_GET_COMPETITION           @"getCompetition"

#define CONSTANT_EMAIL           @"email"
#define CONSTANT_NAME           @"full_name"
#define CONSTANT_FAMILY_NAME          @"family_name"
#define CONSTANT_USER_ID          @"user_id"



#endif /* Constant_h */
