//
//  CategoryCell.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 06/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "CategoryCell.h"
#import <HMSegmentedControl.h>

@implementation CategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) setUpCategory:(NSArray *)nameArray withIndex:(NSInteger) index withWidth:(CGFloat)width{
    _dataArray = @[@"One", @"Two", @"Three",@"four",@"five",@"six",@"seven"];
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:nameArray];
    segmentedControl.frame = CGRectMake(0, 0, width, 66);
    segmentedControl.selectedSegmentIndex = index;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.mainView addSubview:segmentedControl];
}

-(void)segmentedControlChangedValue:(HMSegmentedControl *)segment{
    [self.cellDelegate didTapCategoryWithIndex:segment.selectedSegmentIndex];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
