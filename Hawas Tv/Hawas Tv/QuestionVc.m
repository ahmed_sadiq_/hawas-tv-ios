//
//  QuestionVc.m
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "QuestionVc.h"
#import "OptionsCell.h"
#import "UIImageView+URL.h"
#import "PostService.h"

@interface QuestionVc ()<UITableViewDataSource,UITableViewDelegate>
{
    Competition *compet;
    NSString *selectedAns;
}
@end

@implementation QuestionVc
-(void) setUpView{
    _lblTitle.text = compet.competition_title;
    [_img setImagewithURL:compet.competition_image];
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    compet = [DataManager sharedManager].competition;
    [self setUpView];
    // Do any additional setup after loading the view.
}


#pragma mark - Table View Methods


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return compet.competition_answers.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OptionsCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell==nil) {
        
        cell =[[OptionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    cell.lblOption.text = compet.competition_answers[indexPath.row];
    cell.optImg.image = [UIImage imageNamed:@"Circle"];

    if ([selectedAns isEqualToString: compet.competition_answers[indexPath.row]]) {
        cell.optImg.image = [UIImage imageNamed:@"Circled"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedAns = compet.competition_answers[indexPath.row];
    [_tblView reloadData];
  
}



- (IBAction)btnSubmit:(id)sender {
    if (selectedAns!= nil) {
        [PostService submitCompetitionWithCompetitionID:compet.competition_id Answer:selectedAns FullName:_fName Email:_email Phone:_phone success:^(id data) {
            [self showAlertMessage:data[@"message"]];
            
            [self performSegueWithIdentifier:@"submit" sender:nil];

        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];
        }];
    }
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Hawas!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
