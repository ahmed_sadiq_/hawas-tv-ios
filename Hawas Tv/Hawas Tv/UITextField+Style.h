//
//  UITextField+Style.h
//  Ajo
//
//  Created by Samreen on 27/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Style)


- (void) setRightSpaceSize:(CGFloat) size;

@end
