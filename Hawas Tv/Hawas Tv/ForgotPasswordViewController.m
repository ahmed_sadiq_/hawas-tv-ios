//
//  ForgotPasswordViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 18/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "NetworkManager.h"
#import "Constant.h"
#import <SVProgressHUD.h>

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)proceedPressed:(id)sender {
    if([_tfEmail.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال البريد الإلكتروني"];
    }
    else if(![self validateEmailWithString:_tfEmail.text]){
        [self showAlert:@"تنسيق البريد الإلكتروني غير صحيح"];
    }
    else{
        NSDictionary * postParam = @{
                                     @"request":URI_FORGET_PASSWORD,
                                     @"email":_tfEmail.text,
                                     };
        [self makeNetworkCallWithParam:postParam];
    }
}

#pragma mark - Network Call
-(void) makeNetworkCallWithParam:(NSDictionary *) postParam{
    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        [SVProgressHUD dismiss];
        NSDictionary *responseObj = data[@"user_data"];
        NSString *message = [responseObj objectForKey:@"message"];
        NSLog(@"message: %@",message);
        [self showAlert:@"Success"];
        
    } failure:^(NSError *error) {
        NSLog(@"forgot password error %@",error.localizedDescription);
        [self showAlert:error.localizedDescription];
    }];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"alert");
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
