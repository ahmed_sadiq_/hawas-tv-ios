//
//  BaseService.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "NetworkManager.h"
#import "DataManager.h"

typedef void (^serviceSuccess)(id data);
typedef void (^serviceFailure)(NSError *error);
@interface BaseService : NSObject
//+ (NSString *) currentUserID;
+ (NSError *)createErrorWithLocalizedDescription:(NSString *) localizeDescription;
@end
