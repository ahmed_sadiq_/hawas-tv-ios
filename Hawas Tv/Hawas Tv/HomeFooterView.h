//
//  HomeFooterView.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "Programs.h"
@class HomeFooterView;
@protocol HomeFooterViewDelegate <NSObject>

-(void)didEndCarousel;
-(void)didPressFooterProgramButton:(long) index;
-(void)currentIndexRow:(long) index;

@end

@interface HomeFooterView : UITableViewCell<UIScrollViewDelegate, iCarouselDelegate,iCarouselDataSource>
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *programDescription;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) NSMutableArray *progArray;
@property (assign, nonatomic) BOOL check;
@property (assign, nonatomic) BOOL indexCheck;
@property (assign, nonatomic) NSInteger count;
@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) id<HomeFooterViewDelegate> cellDelegate;
@property (strong, nonatomic) NSArray *featuredArray;

-(void) setUpScrollView:(NSArray *)programsArray;
@end
