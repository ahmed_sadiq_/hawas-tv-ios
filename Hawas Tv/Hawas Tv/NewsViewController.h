//
//  NewsViewController.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 06/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryCell.h"


@interface NewsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CategoryCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSArray *newsArray;
@property (strong, nonatomic) NSArray *headerNews;
@property (strong, nonatomic) NSArray *segmentArray;
@property (strong, nonatomic) NSMutableArray *selectedNewsArray;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (assign, nonatomic) NSInteger count;
@property (assign, nonatomic) BOOL check;
@property (assign, nonatomic) BOOL firstLoad;
@property (assign, nonatomic) BOOL isFirstTime;



@end
