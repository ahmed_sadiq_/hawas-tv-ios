//
//  DataManager.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Competition.h"
#import "FeaturedPrograms.h"

@interface DataManager : NSObject


@property (strong, nonatomic) NSString *authToken;
@property (strong, nonatomic) NSString *deviceToken;

@property (strong, nonatomic) Competition *competition;
@property (strong, nonatomic) NSArray *featuredProgram;
@property (strong, nonatomic) NSArray *latestNews;
@property (strong, nonatomic) NSArray *news;
@property (strong, nonatomic) NSArray *programArray;

+ (DataManager *) sharedManager;

@end
