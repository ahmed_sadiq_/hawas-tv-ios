//
//  SettingVc.m
//  Hawas Tv
//
//  Created by Samreen on 24/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "SettingVc.h"
#import "Constant.h"
#import <SVProgressHUD.h>
#import <Pushwoosh/PushNotificationManager.h>
#import <UserNotifications/UserNotifications.h>

@interface SettingVc ()

@end

@implementation SettingVc
- (IBAction)btnAboutUs:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://hawas.dev.triangle.codes/about-us/"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    BOOL pushStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushStatus"];
    
    if(pushStatus){
        [_pushSwitch setOn:YES];
    }
    else{
        [_pushSwitch setOn:NO];
    }
    
    _isSignin = [[NSUserDefaults standardUserDefaults] objectForKey:@"isSignin"];
    
    if(_isSignin){
        _btnSignup.hidden = YES;
        _btnSignout.hidden = NO;
        _lblUsername.hidden = NO;
        _usernameLine.hidden = NO;
        _editLine.hidden = NO;
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:CONSTANT_NAME];
        _lblUsername.text = [NSString stringWithFormat:@"مرحباً %@",userName];
        _btnEdit.hidden = NO;
        _lblEdit.hidden = NO;
        _topPushConstraint.constant = 186.0;
        _topPushBtnConstraint.constant = 186.0;
    }
    else{
        _btnSignup.hidden = NO;
        _btnSignout.hidden = YES;
        _lblUsername.hidden = YES;
        _usernameLine.hidden = YES;
        _editLine.hidden = YES;
        _btnEdit.hidden = YES;
        _lblEdit.hidden = YES;
        _topPushConstraint.constant = 50.0;
        _topPushBtnConstraint.constant = 50.0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Buttona
- (IBAction)btnSignoutPressed:(id)sender {
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def removeObjectForKey:CONSTANT_EMAIL];
    [def removeObjectForKey:CONSTANT_NAME];
    [def removeObjectForKey:CONSTANT_FAMILY_NAME];
    [def removeObjectForKey:CONSTANT_USER_ID];
    [def removeObjectForKey:@"isSignin"];
    int controllerIndex = 4;
    
    UITabBarController *tabBarController = self.tabBarController;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = [[tabBarController.viewControllers objectAtIndex:controllerIndex] view];
    
    // Transition using a page curl.
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.5
                       options:(controllerIndex > tabBarController.selectedIndex ? UIViewAnimationOptionTransitionCrossDissolve : UIViewAnimationOptionTransitionCrossDissolve)
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = controllerIndex;
                        }
                    }];
}
- (IBAction)btnEditPressed:(id)sender {
    if(_isSignin){
        [self performSegueWithIdentifier:@"editvc" sender:nil];
    }
    else{
        [self showAlert:@"الرجاء تسجيل الدخول"];
    }
}

- (IBAction)switchPressed:(UISwitch *)sender {
    if([sender isOn]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"pushStatus"];
        [[PushNotificationManager pushManager] registerForPushNotifications];
    }
    else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pushStatus"];
        [[PushNotificationManager pushManager] unregisterForPushNotifications];
    }
}
#pragma mark Social Buttons
-(IBAction)fbBtnPressed:(id)sender{
    NSDictionary *options = @{};
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/HawasTvOfficial/"] options:options completionHandler:^(BOOL success) {
    }];
}

- (IBAction)twitterbtnPressed:(id)sender {
    NSDictionary *options = @{};
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/hawastvchannel?lang=en"] options:options completionHandler:^(BOOL success) {
        
    }];
   
}
- (IBAction)instagrambtnPressed:(id)sender {
    NSDictionary *options = @{};
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com/hawastv.official/"] options:options completionHandler:^(BOOL success) {
        
    }];
    
}
- (IBAction)youtubebtnPressed:(id)sender {
    NSDictionary *options = @{};
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/channel/UC1YucH1HLoaEBv5uREw2gvw"] options:options completionHandler:^(BOOL success) {
        
    }];
    
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"alert");
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
