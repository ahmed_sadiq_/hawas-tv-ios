//
//  ProgramDetailCell.h
//  Hawas Tv
//
//  Created by Samreen on 19/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *progImg;
@property (weak, nonatomic) IBOutlet UILabel *progTitle;
@property (weak, nonatomic) IBOutlet UILabel *tfDescription;

@end
