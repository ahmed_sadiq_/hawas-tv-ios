//
//  News.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface News : BaseEntity
@property (strong ,nonatomic) NSString *categoryID;
@property (strong ,nonatomic) NSString *categoryName;
@property (strong ,nonatomic) NSArray *newsArray;

-(id)initWithDictionary:(NSDictionary *)responseData;
+(NSArray *)mapNewsFromArray:(NSArray *)arrlist;

@end
