//
//  EditViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "EditViewController.h"
#import "UITextField+Style.h"
#import "Constant.h"
#import "NetworkManager.h"
#import <SVProgressHUD.h>

@interface EditViewController ()<UITextFieldDelegate>

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self initialization];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initialization{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    _tfName.text = [def objectForKey:CONSTANT_NAME];
    _tfFname.text = [def objectForKey:CONSTANT_FAMILY_NAME];
    _tfEmail.text = [def objectForKey:CONSTANT_EMAIL];
    [_tfName setRightSpaceSize:10];
    [_tfFname setRightSpaceSize:10];
    [_tfEmail setRightSpaceSize:10];
}

#pragma mark - TextField Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag == 3){
        [_scrollView setContentOffset:CGPointMake(0, 150) animated:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

-(void)dismissKeyboard {
    [_tfName resignFirstResponder];
    [_tfFname resignFirstResponder];
    [_tfEmail resignFirstResponder];
}
#pragma mark - Btn Delegate

- (IBAction)btnBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSavePressed:(id)sender {
    NSCharacterSet *familySet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
    
    NSCharacterSet * nameSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "] invertedSet];
    
    
    
    if([_tfName.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال الاسم" isUpdate:NO];
    }
    else if ([_tfName.text rangeOfCharacterFromSet:nameSet].location != NSNotFound) {
        [self showAlert:@"تنسيق الاسم غير صحيح" isUpdate:NO];
    }
    else if([_tfFname.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال الشهرة" isUpdate:NO];
    }
    else if ([_tfFname.text rangeOfCharacterFromSet:familySet].location != NSNotFound) {
        [self showAlert:@"تنسيق الشهرة غير صحيح" isUpdate:NO];
    }
    else if([_tfEmail.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال البريد الإلكتروني" isUpdate:NO];
    }
    else if(![self validateEmailWithString:_tfEmail.text]){
        [self showAlert:@"تنسيق البريد الإلكتروني غير صحيح" isUpdate:NO];
    }
    else{
        NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:CONSTANT_USER_ID];
        NSDictionary *postParam = @{@"request":URL_EDIT_PROFILE,
                                    CONSTANT_USER_ID:userID,
                                    CONSTANT_NAME:_tfName.text,
                                    CONSTANT_FAMILY_NAME: _tfFname.text,
                                    CONSTANT_EMAIL: _tfEmail.text};
        [self makeServerCallWithParams:postParam];
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - Network Call
- (void) makeServerCallWithParams:(NSDictionary *)postParam{
    
    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        [SVProgressHUD dismiss];
        NSDictionary *responseObj = data[@"user_data"];
        NSString *message = [responseObj objectForKey:@"message"];
        NSLog(@"edit: %@",responseObj);
        
        NSString *email = [responseObj objectForKey:CONSTANT_EMAIL];
        NSString *name = [responseObj objectForKey:CONSTANT_NAME];
        NSString *fName = [responseObj objectForKey:CONSTANT_FAMILY_NAME];
        NSString *userID = [responseObj objectForKey:CONSTANT_USER_ID];
        
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:CONSTANT_EMAIL];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:CONSTANT_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:fName forKey:CONSTANT_FAMILY_NAME];
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:CONSTANT_USER_ID];
        
        [self showAlert:message isUpdate:YES];
        
    } failure:^(NSError *error) {
        NSLog(@"Failure: %@",error.localizedDescription);
        [self showAlert:error.localizedDescription isUpdate:YES];
    }];
}
#pragma mark - Alert Method
-(void)showAlert:(NSString *)message isUpdate:(BOOL)update{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(update){
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
