//
//  EpisodeVC.h
//  Hawas Tv
//
//  Created by Samreen on 19/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+URL.h"
#import "PostService.h"
#import "Episode.h"
#import "HomeCell.h"
#import "ProgramsFooterCell.h"
#import "Constant.h"
#import "HeaderCell.h"

@interface EpisodeVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *videoBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoBoxHeightConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *pauseBtn;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *videoImg;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSMutableArray *episodesArray;
@property (strong, nonatomic) Episode *selectedEpisod;
@property (strong, nonatomic) NSString *progId;
@property (strong, nonatomic) NSString *programTitle;

@end
