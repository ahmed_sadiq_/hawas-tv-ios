//
//  ViewController.h
//  Hawas Tv
//
//  Created by Samreen on 17/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (assign, nonatomic) NSInteger count;
//@property (assign, nonatomic) BOOL intenetCheck;


@end

