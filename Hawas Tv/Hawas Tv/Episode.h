//
//  Episode.h
//  Hawas Tv
//
//  Created by Samreen on 19/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "BaseEntity.h"

@interface Episode : BaseEntity
@property (strong, nonatomic) NSString *episode_title;
@property (strong, nonatomic) NSString *episode_id;
@property (strong, nonatomic) NSString *episode_thumbnail;
@property (strong, nonatomic) NSString *episode_video_url;
@property (strong, nonatomic) NSString *episode_uploaded_date;
@property (strong, nonatomic) NSArray *episode_modification_date;

- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapEpisodFromArray:(NSArray *)arrlist ;
@end
