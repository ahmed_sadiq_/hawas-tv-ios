//
//  UIImageView+URL.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URL)

- (void) setImagewithURL:(NSString *) strURL;
- (void) setSignImageWithURL:(NSString *) strURL;

@end

