//
//  PostService.h
//  Ajo
//
//  Created by Samreen Noor on 04/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"
 
@interface PostService : BaseService
+(void) getProgramsWithPageNumber:(NSString *)pageNo
                         PageSize:(NSString *)pagesize
                          success:(serviceSuccess)success
                          failure:(serviceFailure)failure;
+(void) getEpisodesWithProgramID:(NSString *)pId
                      PageNumber:(NSString *)pageNo
                        PageSize:(NSString *)pagesize
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure;

+(void) submitCompetitionWithCompetitionID:(NSString *)cId
                                    Answer:(NSString *)answer
                                  FullName:(NSString *)fullName
                                     Email:(NSString *)email
                                     Phone:(NSString *)phone
                                   success:(serviceSuccess)success
                                   failure:(serviceFailure)failure;
+(void) getCompetitionSuccess:(serviceSuccess)success
                      failure:(serviceFailure)failure;

+(void) getNewsWithCategoryID:(NSString *)catID
                       Pageno:(NSString *) pageno
                     PageSize:(NSString *) pagesize
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure;

+(void) getNewsSuccess:(serviceSuccess)success
                      failure:(serviceFailure)failure;

@end
