
//
//  WalkThrough.m
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "WalkThrough.h"
#import "Constant.h"

@interface WalkThrough ()<UIScrollViewDelegate>

@end

@implementation WalkThrough

- (void)viewDidLoad {
    [super viewDidLoad];
    BOOL isFirstTimeRun = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTimeRun"];
    if (!isFirstTimeRun) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstTimeRun"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"pushStatus"];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"showCount"];
        [self performSegueWithIdentifier:@"home" sender:nil];
    }
    
    if (isFirstTimeRun) {
        
        [self performSegueWithIdentifier:@"home" sender:nil];
    }
    // Do any additional setup after loading the view.
}

-(void)viewDidLayoutSubviews{
    
    CGFloat scrollViewHeight = self.scrollView.frame.size.height;
    CGFloat scrollViewWidth = self.scrollView.frame.size.width;
 
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth * 3, scrollViewHeight);
    self.scrollView.delegate = self;
    self.pageControll.currentPage = 0;
    if(IS_IPHONE_6Plus)
    {
        _viewWidthConstrain.constant = 414;
        
    }
    else if(IS_IPHONE_5)
    {
        _viewWidthConstrain.constant = 320;
  
    }
    else if (IS_IPAD){
      _viewWidthConstrain.constant = 768;
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
 

}
- (IBAction)btnCross:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstTimeRun"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"pushStatus"];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.pageControll.currentPage = page;

}




//- (IBAction)btnGetStart:(id)sender {
//    
//    ViewController *viewContr = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
//    [self.navigationController pushViewController:viewContr animated:YES];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
