//
//  ContactUs.m
//  Hawas Tv
//
//  Created by Samreen on 24/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "ContactUs.h"
#import "UITextField+Style.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactUs ()<UITextViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    UITextField *currentTextField;
    UITextView *currentTextView;
}
@end

@implementation ContactUs

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
    
    
    [[self.txtDescription layer] setBorderColor:[[UIColor colorWithRed:164.0/255.0 green:70.0/255.0 blue:194.0/255.0 alpha:1] CGColor]];
    [[self.txtDescription layer] setBorderWidth:1];
    [[self.txtDescription layer] setCornerRadius:1];
    [_tfEmail setRightSpaceSize:10];
    [_tfCity setRightSpaceSize:10];
    [_tfName setRightSpaceSize:10];

}

-(void)dismissKeyboard {
    [currentTextField resignFirstResponder];
    [_txtDescription resignFirstResponder];
}
- (IBAction)btnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)btnSend:(id)sender {
    if([_tfName.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال الاسم"];
    }
    else if([_tfCity.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال البلد"];
    }
    else if([_tfEmail.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال البريد الإلكتروني"];
    }
    else if(![self emailValidate:_tfEmail.text]){
        [self showAlert:@"تنسيق البريد الإلكتروني غير صحيح"];
    }
    else if([_txtDescription.text isEqualToString:@""]){
        [self showAlert:@"الرجاء إدخال رسالتك"];
    }
    else if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        NSString *detail = [NSString stringWithFormat:@"%@,\n%@,\n%@,\n%@",_tfName.text,_tfCity.text,_tfEmail.text,_txtDescription.text];
        [mailCont setSubject:@""];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"info@hawastv.com"]];
        [mailCont setMessageBody:detail isHTML:NO];
        
        [self presentModalViewController:mailCont animated:YES];
    }
    else{
        NSLog(@"cannot send mail");
    }
}
    - (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
    {
        switch (result) {
            case MFMailComposeResultSent:
                NSLog(@"You sent the email.");
                break;
            case MFMailComposeResultSaved:
                NSLog(@"You saved a draft of this email");
                break;
            case MFMailComposeResultCancelled:
                NSLog(@"You cancelled sending this email.");
                break;
            case MFMailComposeResultFailed:
                NSLog(@"Mail failed:  An error occurred when trying to compose this email");
                break;
            default:
                NSLog(@"An error occurred when trying to compose this email");
                break;
        }
        
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    
    
    if( [_tfName.text length] >= 1 && [_tfCity.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES ){
        [_btnSend setEnabled:YES];
    }
    if (_tfName.text.length==0 || _tfCity.text.length==0||_tfEmail.text.length==0) {
        //[self showAlertMessage:@"يرجى إدخال معلومات صحيحة"];
       // [_btnSend setEnabled:NO];
        
        return NO;
    }
    else if (![self emailValidate:_tfEmail.text]) {
        // [self showAlertMessage:@"الرجاء إدخال بريد إلكتروني صحيح"];
        //[_btnSend setEnabled:NO];
        
        return NO;
    }
    
    
    
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    currentTextField  = textField;
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self checktextFields];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
    [_scrollView setContentOffset:CGPointMake(0, 150) animated:YES];

}
-(void)textViewDidEndEditing:(UITextView *)textView{

    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];

}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ في التحقق" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"موافقة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"alert");
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
