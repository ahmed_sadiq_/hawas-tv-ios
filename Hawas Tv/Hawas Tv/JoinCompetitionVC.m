//
//  JoinCompetitionVC.m
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "JoinCompetitionVC.h"
#import "UITextField+Style.h"
#import "QuestionVc.h"

@interface JoinCompetitionVC ()<UITextViewDelegate>
{
    
    UITextField *currentTextField;
}
@end

@implementation JoinCompetitionVC

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([segue.identifier isEqualToString:@"start" ]) {
        QuestionVc  *qVC = segue.destinationViewController;
        qVC.fName = _tfName.text;
        qVC.email = _tfEmail.text;
        qVC.phone = _tfPhone.text;
        
    }
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _compet = [DataManager sharedManager].competition;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    [_tfEmail setRightSpaceSize:20];
    [_tfPhone setRightSpaceSize:20];
    [_tfName setRightSpaceSize:20];
    
    // Do any additional setup after loading the view.
}


-(void)dismissKeyboard {
//    self.confirmationView.hidden = YES;
    [currentTextField resignFirstResponder];
    
}

#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    
    
    if( [_tfName.text length] >= 1 && [_tfPhone.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES ){
        //            [_btnStart setEnabled:YES];
        willEnabled = YES;
    }
    if (_tfName.text.length==0 || _tfPhone.text.length==0||_tfEmail.text.length==0) {
        [self showAlertMessage:@"يرجى إدخال معلومات صحيحة"];
        //            [_btnStart setEnabled:NO];
        
        return NO;
    }
    else if (![self emailValidate:_tfEmail.text]) {
        [self showAlertMessage:@"الرجاء إدخال بريد إلكتروني صحيح"];
        //            [_btnStart setEnabled:NO];
        
        return NO;
    }
    
    
    
    
    return willEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    currentTextField  = textField;
    [self animateTextField:nil up:YES];
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:nil up:NO];
    //[self checktextFields];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)confirmationViewBtnPressed:(id)sender{
    if ([self checktextFields]){
        [self dismissKeyboard];
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             self.confirmationView.hidden = NO;
                             self.confirmationView.frame = CGRectMake(0, self.view.frame.size.height - 266 , self.view.frame.size.width, 226);
                         }
                         completion:^(BOOL finished){
                         }];
    }
}

- (IBAction)btnStartNow:(id)sender {

  [self performSegueWithIdentifier:@"start" sender:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Hawas!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
