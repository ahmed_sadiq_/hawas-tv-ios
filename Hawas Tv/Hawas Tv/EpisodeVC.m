//
//  EpisodeVC.m
//  Hawas Tv
//
//  Created by Samreen on 19/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "EpisodeVC.h"


@interface EpisodeVC ()<UIWebViewDelegate>
{
   int page;
    BOOL ismoreData;
    BOOL serverCall;
    HeaderCell *headerView;
}
@end

@implementation EpisodeVC

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    _webView.hidden = NO;
    [_loading stopAnimating];
    _pauseBtn.hidden = YES;
    NSLog(@"finish.......");

}
-(void) setUpPlayerView{
    

    
    NSString *url;
    if ([_selectedEpisod.episode_video_url isEqualToString:@""] ||_selectedEpisod.episode_video_url== nil ) {
        url=@"http://vod-platform.net/Embed/Wmfw7xJU4nW9QXlxA78Gw?responsive=1&autoplay=1";

    }
    else{
        [_videoImg setImagewithURL:_selectedEpisod.episode_thumbnail];
        [_loading startAnimating];
        _webView.hidden = YES;
        url=_selectedEpisod.episode_video_url;

    }
    
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    self.webView.scrollView.bounces = NO;
    [self.webView setMediaPlaybackRequiresUserAction:NO];
    
    
}

-(void)viewDidLayoutSubviews{
    if (IS_IPHONE_5) {
        _videoBoxHeightConstrain.constant = 189;
    }
    else if (IS_IPHONE_6Plus){
        _videoBoxHeightConstrain.constant = 232;

    }
    else if (IS_IPAD){
        _videoBoxHeightConstrain.constant = 430;

    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupRefreshControl];
    [self setUpPlayerView];
    ismoreData = YES;
    serverCall =NO;
    //[self getEpisodes];
    page = 1;
    self.tblView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);

    
    // Do any additional setup after loading the view.
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
   
}
- (IBAction)btnMore:(id)sender {
    if (ismoreData && !serverCall) {
        page++;
        [self getEpisodes];
    }
    
}


#pragma mark - Table View Methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    
    static NSString *HeaderCellIdentifier = @"header";
    headerView = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    
    
    if (headerView == nil) {
        headerView = [[HeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    headerView.lblTitle.text = self.programTitle;
    headerView.lblEpisod.text = _selectedEpisod.episode_title;
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 186;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
 
    static NSString *footerCellIdentifier = @"footer";
    ProgramsFooterCell *footerView = [tableView dequeueReusableCellWithIdentifier:footerCellIdentifier];
    
    
    if (footerView == nil) {
        footerView = [[ProgramsFooterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:footerCellIdentifier];
    }
    
    if(_episodesArray.count < 4){
        footerView.btnMore.hidden = YES;
        footerView.lbl.hidden = YES;
        footerView.back.hidden = YES;
        footerView.plus.hidden = YES;
    }else{
        footerView.btnMore.hidden = NO;
        footerView.lbl.hidden = NO;
        footerView.back.hidden = NO;
        footerView.plus.hidden = NO;

    }
    
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(_episodesArray.count <4){
        return 0;
    }
    
    return 57;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _episodesArray.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Episode *ep = _episodesArray[indexPath.row];
    HomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell==nil) {
        
        cell =[[HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    [cell.postImg setImagewithURL: ep.episode_thumbnail];
    cell.lblTitle.text = ep.episode_title;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 70;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectedEpisod =_episodesArray[indexPath.row];
    [self setUpPlayerView];
    
}




-(void) getEpisodes{
    serverCall = YES;

    NSString *pageStr = [NSString stringWithFormat:@"%d",page];
    [PostService getEpisodesWithProgramID:_progId PageNumber:pageStr PageSize:@"4" success:^(id data) {

        
        
        //////////////
        
        
        [self.refreshControl endRefreshing];
        NSArray *tempPost = data;
        if (tempPost.count == 0) {
            ismoreData = NO;
            serverCall = NO;

        }
        else{
        if (page==1) {
            [_episodesArray removeAllObjects];
            
            _episodesArray = data;
            [_tblView reloadData];
            serverCall = NO;
            ismoreData = YES;
        }
        else{
            for (Episode *epis in tempPost) {
                [_episodesArray addObject:epis];
            }
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            int startIndex = (page-1) *4;
            for (int i = startIndex ; i < startIndex+4; i++) {
                if(i<_episodesArray.count) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
            }
            [_tblView beginUpdates];
            [_tblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            [_tblView endUpdates];
            serverCall = NO;
            ismoreData = YES;

        }
        }
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_episodesArray.count-3 inSection:0];
        [_tblView scrollToRowAtIndexPath:indexPath
                             atScrollPosition:UITableViewScrollPositionTop
                                     animated:YES];
    } failure:^(NSError *error) {
        
    }];
    
}
- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}
- (void)refreshHome:(id)sender
{
    page=1;
    if (!serverCall) {
        
        [self getEpisodes];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
