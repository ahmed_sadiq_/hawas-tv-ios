//
//  LatestNews.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface LatestNews : BaseEntity
@property (strong ,nonatomic) NSString *newsID;
@property (strong ,nonatomic) NSString *newsCatName;
@property (strong ,nonatomic) NSString *newsTitle;
@property (strong ,nonatomic) NSString *newsDescription;
@property (strong ,nonatomic) NSString *newsImage;


-(id) initWithDictionary:(NSDictionary *) responseData;
+(NSArray* ) mapNewsFromArray:(NSArray *) arrlist;
@end
