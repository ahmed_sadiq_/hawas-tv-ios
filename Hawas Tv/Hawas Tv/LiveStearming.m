//
//  LiveStearming.m
//  Hawas Tv
//
//  Created by Samreen on 23/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "LiveStearming.h"
#import "SVProgressHUD.h"

@interface LiveStearming ()<UIWebViewDelegate>

@end

@implementation LiveStearming

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [SVProgressHUD show];
    NSString *url=@"https://hawastv.com/live_streaming.php";
        
 
    
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    self.webView.scrollView.bounces = NO;
    [ self.webView setAllowsInlineMediaPlayback:YES];

    [self.webView setMediaPlaybackRequiresUserAction:NO];
    _webView.opaque = NO;
   _webView.backgroundColor = [UIColor clearColor];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [SVProgressHUD show];

    NSString *url=@"https://hawastv.com/live_streaming.php";
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_webView loadHTMLString:@"" baseURL:nil];

}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
