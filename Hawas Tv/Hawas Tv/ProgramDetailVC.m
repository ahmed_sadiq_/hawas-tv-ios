//
//  ProgramDetailVC.m
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "ProgramDetailVC.h"
#import "HomeCollectionViewCell.h"
#import "UIImageView+URL.h"
#import "ProgramDetailCell.h"
#import "EpisodeVC.h"
#import "Episode.h"

@interface ProgramDetailVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    Episode *selectedEpisod;
}
@end

@implementation ProgramDetailVC



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([segue.identifier isEqualToString:@"eDetail" ]) {
        EpisodeVC  *eVC = segue.destinationViewController;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        if(_episodsArray.count>=4){
            for (int i = 0; i<4; i++) {
                tempArray[i] = _episodsArray[i];
            }
            eVC.episodesArray = tempArray;
        }
        else
           eVC.episodesArray = _episodsArray.mutableCopy;
        
        eVC.selectedEpisod = selectedEpisod;
        eVC.progId = _selectProgram.programId;
        eVC.programTitle = _selectProgram.programTitle;
    }
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _boxView.layer.cornerRadius = 20;
    _boxView.layer.masksToBounds = YES;
    _lblProgramTitle.text = _selectProgram.programTitle;

    [self getEpisodes];
    // Do any additional setup after loading the view.
}


- (void)viewDidAppear:(BOOL)animate
{
    [super viewDidAppear:animate];
}

- (IBAction)shareButton:(id)sender {

    NSString *textToShare = _selectProgram.programDescription;
    NSURL *myWebsite = [NSURL URLWithString:_selectProgram.programImg];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - TableView Methods

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
    
    
}
- (NSString *)stringByStrippingHTML {
    NSRange r;
    NSString *s = _selectProgram.programDescription ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProgramDetailCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell==nil) {
        
        cell =[[ProgramDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    [cell.progImg setImagewithURL: _selectProgram.programImg];
   
    NSString * timeStr = [_selectProgram.programTiming  componentsJoinedByString:@","];

    cell.progTitle.text = timeStr;

    cell.tfDescription.text =_selectProgram.programDescription ;
    [cell.tfDescription sizeToFit];

     //cell.tfDescription.textContainerInset = UIEdgeInsetsZero;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CGSize size = [_selectProgram.programDescription sizeWithFont:[UIFont fontWithName:@"Cairo-Bold" size:17] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    NSLog(@"%f",size.height);
    
    return 270 + size.height ;
    

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_episodsArray.count>0){
    [self performSegueWithIdentifier:@"eDetail" sender:nil];
    }
    
}



#pragma mark - collectionView Methods


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //return [self.dataArray count];
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return _episodsArray.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Episode *episod = _episodsArray[indexPath.row];
    HomeCollectionViewCell * cell = (HomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeCollectionViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    [cell.postImg setImagewithURL: episod.episode_thumbnail];
    cell.lblTitle.text = episod.episode_title;
    
    return cell;
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return CGSizeMake(184, 140);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Episode *episod = _episodsArray[indexPath.row];
    selectedEpisod =episod;
    [self performSegueWithIdentifier:@"eDetail" sender:nil];

}


-(void) getEpisodes{
    [PostService getEpisodesWithProgramID:_selectProgram.programId PageNumber:@"1" PageSize:@"10" success:^(id data) {
        _episodsArray = data;
        _episodsArray = [[_episodsArray reverseObjectEnumerator] allObjects];
        [_collectionView reloadData];
        if (_episodsArray.count>0) {
            selectedEpisod = _episodsArray[0];

        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
