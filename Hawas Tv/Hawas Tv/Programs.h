//
//  Programs.h
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "BaseEntity.h"

@interface Programs : BaseEntity
@property (strong, nonatomic) NSString *programTitle;
@property (strong, nonatomic) NSString *programId;
@property (strong, nonatomic) NSString *programDescription;
@property (strong, nonatomic) NSString *programImg;
@property (strong, nonatomic) NSString *programFolderId;
@property (strong, nonatomic) NSArray *programTiming;




- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapProgramFromArray:(NSArray *)arrlist;
@end
