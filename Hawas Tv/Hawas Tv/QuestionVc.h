//
//  QuestionVc.h
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Competition.h"
#import "DataManager.h"

@interface QuestionVc : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSString *fName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;

@end
