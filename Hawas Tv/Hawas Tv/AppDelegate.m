//
//  AppDelegate.m
//  Hawas Tv
//
//  Created by Samreen on 17/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "AppDelegate.h"
#import <Pushwoosh/PushNotificationManager.h>
#import <UserNotifications/UserNotifications.h>
#import <ALAlertBanner.h>
#import <ALAlertBannerManager.h>
#import <ISMessages/ISMessages.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:@"Cairo-Bold" size:10.0f]
                                                        } forState:UIControlStateNormal];
    
    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
    pushManager.delegate = self;
    
    // set default Pushwoosh delegate for iOS10 foreground push handling
    [UNUserNotificationCenter currentNotificationCenter].delegate = [PushNotificationManager pushManager].notificationCenterDelegate;
    
    // track application open statistics
    [[PushNotificationManager pushManager] sendAppOpen];
    
    BOOL pushStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushStatus"];
    BOOL isFirstTimeRun = [[NSUserDefaults standardUserDefaults] valueForKey:@"isFirstTimeRun"];
    
    if(!isFirstTimeRun){
        [[PushNotificationManager pushManager] registerForPushNotifications];
    }
    else{
         BOOL pushStatus = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushStatus"];
        if(pushStatus){
             [[PushNotificationManager pushManager] registerForPushNotifications];
        }
        else{
             [[PushNotificationManager pushManager] unregisterForPushNotifications];
        }
       // [_pushSwitch setOn:NO];
    }
    
    // register for push notifications!
   

 
    return YES;
}


// system push notification registration success callback, delegate to pushManager
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
}

// system push notification registration error callback, delegate to pushManager
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[PushNotificationManager pushManager] handlePushRegistrationFailure:error];
}

// system push notifications callback, delegate to pushManager
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    [[PushNotificationManager pushManager] handlePushReceived:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)onPushReceived:(PushNotificationManager *)pushManager withNotification:(NSDictionary *)pushNotification onStart:(BOOL)onStart {
    NSLog(@"%@", [[pushNotification objectForKey:@"aps"] objectForKey:@"alert"]);
    
    NSDictionary *value = [[pushNotification objectForKey:@"aps"] objectForKey:@"alert"];
    NSString *title;
    NSString *body;
    //[[value objectAtIndex:indexPathSet.row] valueForKey:@"SetEntries"] != nil
    if([value isKindOfClass:[NSDictionary class]]){
        if ([value objectForKey:@"title"] != nil) {
            title = [value objectForKey:@"title"];
        } else {
            NSLog(@"No object set for key @\"b\"");
        }
        if ([value objectForKey:@"body"] != nil) {
            body = [value objectForKey:@"body"];
            NSLog(@"There's an object set for key @\"b\"!");
        } else {
            NSLog(@"No object set for key @\"b\"");
        }
    }
    else{
        body =(NSString *) value;
    }

    
    NSLog(@"Push notification received");
//    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    ALAlertBanner *banner = [ALAlertBanner alertBannerForView:appDelegate.window
//                                                        style:ALAlertBannerStyleSuccess
//                                                     position:ALAlertBannerPositionTop
//                                                        title:title
//                                                     subtitle:body];
//    
//    
//
//    [banner show];
    
    [ISMessages showCardAlertWithTitle:title
                               message:body
                              duration:3.f
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeSuccess
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
 
}

// this event is fired when user clicks on notification
- (void)onPushAccepted:(PushNotificationManager *)pushManager withNotification:(NSDictionary *)pushNotification onStart:(BOOL)onStart {
    NSLog(@"Push notification accepted");
    // shows a user tapped the notification. Implement user interaction, such as showing push details
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
