//
//  HomeViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeHeaderView.h"
#import "Programs.h"
#import "HomeCell.h"
#import "UIImageView+URL.h"
#import "PostService.h"
#import "HomeFooterView.h"
#import "NewsDetailViewController.h"
#import "LatestNews.h"
#import "ProgramDetailVC.h"
#import <SVProgressHUD.h>
#import "Reachability.h"
@interface HomeViewController ()<ProgramHeaderViewDelegate>
{
    NSMutableArray *programsAarray;
    NSArray *tempProgramsAarray;
    int pageNo;
    BOOL dataArrayISEmpty;
    BOOL serverCall;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    programsAarray = [[NSMutableArray alloc]init];
    tempProgramsAarray = [[NSArray alloc]init]; 
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *savedata = [defaults objectForKey:@"programsArray"];
    programsAarray = [NSKeyedUnarchiver unarchiveObjectWithData:savedata];
    
    NSData *latestdata = [defaults objectForKey:@"latestNews"];
    _latestNewsArray = [NSKeyedUnarchiver unarchiveObjectWithData:latestdata];
    
    NSData *featureddata = [defaults objectForKey:@"featuredArray"];
    _featuredArray = [NSKeyedUnarchiver unarchiveObjectWithData:featureddata];
    
    [DataManager sharedManager].programArray = programsAarray;
    [DataManager sharedManager].featuredProgram = _featuredArray;
    [DataManager sharedManager].latestNews = _latestNewsArray;
    [defaults synchronize];
    [self setupRefreshControl];
    pageNo = 1;
    _check = 0;
    dataArrayISEmpty =NO;
    serverCall = NO;
    
    // check if installed for the first time
    NSInteger showCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"showCount"];
    _firstTimeCount = showCount;
    if(showCount > 0 && showCount <=2){
        // not first time
        _tblView.hidden = NO;
    }
    else{
        // first time
        if(![self internetAvailable]) {
            _tblView.hidden = YES;
            [self showAlert:@"يرجى التحقق من الاتصال بالإنترنت!"];
        } else {
            //internet connection
            _check = -1;
            _tblView.hidden = YES;
            showCount = 1;
            [[NSUserDefaults standardUserDefaults] setInteger:showCount forKey:@"showCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    [self getPrograms];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didPressProgramDetailedButton:(NSInteger )tag{
    Programs *prog = programsAarray[tag];
    [self performSegueWithIdentifier:@"detail" sender:prog];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}
#pragma mark - TableView Delegates

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *HeaderCellIdentifier = @"header";
    HomeHeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    if (headerView == nil) {
        headerView = [[HomeHeaderView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    headerView.headerDelegate = self;
    headerView.isFeatured = YES;
    headerView.isLatesNews = NO;
    headerView.isShadowOverlay = NO;
    [headerView setUpScrollView:_featuredArray.mutableCopy];
    
    return headerView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    static NSString *footerCellIdentifier = @"footer";
    _footerView = [tableView dequeueReusableCellWithIdentifier:footerCellIdentifier];
    if (_footerView == nil) {
        _footerView = [[HomeFooterView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:footerCellIdentifier];
    }
    _footerView.cellDelegate = self;
    _footerView.index = _count;
    
    if(_check < 2){
        _footerView.check = true;
        _footerView.indexCheck = false;
        if(_firstTimeCount == 0) {
            _check++;
        } else {
            _check = 2;
        }
        
    }
    else{
        _footerView.indexCheck = true;
        _footerView.check = false;
    }
//    tempProgramsAarray =  [[programsAarray reverseObjectEnumerator] allObjects];
    [_footerView setUpScrollView:tempProgramsAarray];
    return _footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 294;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 294;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _latestNewsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LatestNews *news = _latestNewsArray[indexPath.row];
    HomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell==nil) {
        cell =[[HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [cell.postImg setImagewithURL: news.newsImage];
    cell.lblTitle.text = news.newsTitle;
    cell.lblCategory.text = news.newsCatName;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 280;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LatestNews *lNews = _latestNewsArray[indexPath.row];
    
    [self performSegueWithIdentifier:@"newsdetail" sender:lNews];
}

-(void) getPrograms{
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        serverCall = YES;
        _progCount = programsAarray.count;
        NSString *page = [NSString stringWithFormat:@"%d", pageNo];
        [PostService getProgramsWithPageNumber:page PageSize:@"10" success:^(id data) {
            
            [self.refreshControl endRefreshing];
            _tblView.hidden = NO;
            NSArray *tempPost = data;
            NSMutableArray *tempArray = tempPost.mutableCopy;
            [DataManager sharedManager].programArray = tempPost;
            _latestNewsArray = [DataManager sharedManager].latestNews;
            [DataManager sharedManager].programArray = tempArray;
            _featuredArray = [DataManager sharedManager].featuredProgram;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *savedata = [NSKeyedArchiver archivedDataWithRootObject:tempArray];
            [defaults setObject:savedata forKey:@"programsArray"];
            NSData *latestdata = [NSKeyedArchiver archivedDataWithRootObject:_latestNewsArray];
            [defaults setObject:latestdata forKey:@"latestNews"];
            
            NSData *featureddata = [NSKeyedArchiver archivedDataWithRootObject:_featuredArray];
            [defaults setObject:featureddata forKey:@"featuredArray"];
            
            [defaults synchronize];
            
            
            if (tempPost.count == 0) {
                dataArrayISEmpty = YES;
            }
            if (pageNo==1) {
                _count = 0;
                [programsAarray removeAllObjects];
                
                programsAarray = data;
                tempProgramsAarray =  [[programsAarray reverseObjectEnumerator] allObjects];

                [_tblView reloadData];
                serverCall = NO;
                dataArrayISEmpty = NO;
            }
            else{
                _count++;
                for (Programs *place in tempPost) {
                    [programsAarray addObject:place];
                }
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
                int startIndex = (pageNo-1) *10;
                for (int i = startIndex ; i < startIndex+10; i++) {
                    if(i<programsAarray.count) {
                        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    }
                }
                if(programsAarray.count != _progCount){
                    tempProgramsAarray =  [[programsAarray reverseObjectEnumerator] allObjects];
                    _footerView.index = _count;
                    _footerView.check = true;
                    [_footerView setUpScrollView:tempProgramsAarray];
                }
                
                serverCall = NO;
                
            }
            NSLog(@"%@",data);
        } failure:^(NSError *error) {
            NSLog(@"%@",error.localizedDescription);
            
        }];
}
- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}
- (void)refreshHome:(id)sender
{
    pageNo=1;
    if (!serverCall) {
        [self getPrograms];
    }
}

#pragma mark - FooterView Delegate

-(void)didEndCarousel{
    if ( !serverCall && !dataArrayISEmpty) {
        pageNo++;
        [self getPrograms];
    }
}

-(void)didPressFooterProgramButton:(long)index{
    Programs *prog = tempProgramsAarray[index];
    [self performSegueWithIdentifier:@"detail" sender:prog];
}

-(void)currentIndexRow:(long)index{
    _count = index;
}

#pragma mark - InternetConnection Method

- (BOOL)internetAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"إعادة المحاولة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(![self internetAvailable]) {
            [self showAlert:@"يرجى التحقق من الاتصال بالإنترنت!"];
        } else {
            _check = -1;
            _tblView.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"showCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
             [self getPrograms];
        }
    }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"newsdetail"]){
        NewsDetailViewController *vc = segue.destinationViewController;
        vc.lNews = sender;
    }
    if([segue.identifier isEqualToString:@"detail"]){
        ProgramDetailVC  *progVC = segue.destinationViewController;
        progVC.selectProgram = sender;
    }
}


@end
