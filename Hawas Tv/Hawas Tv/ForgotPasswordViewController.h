//
//  ForgotPasswordViewController.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 18/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@end
