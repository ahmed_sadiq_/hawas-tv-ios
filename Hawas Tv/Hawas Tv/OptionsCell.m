//
//  OptionsCell.m
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "OptionsCell.h"

@implementation OptionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
