//
//  ProgramDetailVC.h
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Programs.h"

@interface ProgramDetailVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) Programs *selectProgram;
@property (strong, nonatomic) NSArray *episodsArray;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end
