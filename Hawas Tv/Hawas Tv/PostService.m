//
//  PostService.m
//  Ajo
//
//  Created by Samreen Noor on 04/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PostService.h"
#import "Programs.h"
#import "FeaturedPrograms.h"
#import "Episode.h"
#import "SVProgressHUD.h"
#import "Competition.h"
#import "DataManager.h"
#import "LatestNews.h"

@implementation PostService


+(void) getProgramsWithPageNumber:(NSString *)pageNo
                     PageSize:(NSString *)pagesize
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{
    


    NSDictionary * postParam = @{
                                    @"page_no":pageNo,
                                    @"page_size":pagesize,
                                    @"request":URL_GET_PROGRAMS
                                    
                                    };
    
    [NetworkManager postURI:@""
                 parameters:postParam
                    success:^(id data) {
                        
                            [SVProgressHUD dismiss];
                        BOOL status=data[@"flag"];
                        
                        if (status) {
                             NSArray *programsArray = [[NSArray alloc]init];
                           programsArray = [Programs  mapProgramFromArray:data[@"programs"]] ;
                            if([pageNo isEqualToString:@"1"]){
                                NSArray *featuredArray = [[NSArray alloc] init];
                                featuredArray = [FeaturedPrograms mapProgramFromArray:data[@"featured_programs"]];
                                
                                NSArray *latestNewsArray = [[NSArray alloc] init];
                                latestNewsArray = [LatestNews mapNewsFromArray:data[@"latest_news"]];
                                
                                [DataManager sharedManager].featuredProgram = featuredArray;
                                [DataManager sharedManager].latestNews = latestNewsArray;
                            }
                            
                            [DataManager sharedManager].competition = [[Competition alloc]initWithDictionary:data[@"competition"]];
                            success (programsArray);
                       
                        
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) getEpisodesWithProgramID:(NSString *)pId
                      PageNumber:(NSString *)pageNo
                         PageSize:(NSString *)pagesize
                          success:(serviceSuccess)success
                          failure:(serviceFailure)failure{
    
    
    
    NSDictionary * postParam = @{
                                 @"program_id":pId,
                                 @"page_no":pageNo,
                                 @"page_size":pagesize,
                                 @"request":URL_GET_EPISODES
                                 
                                 };
    
    [NetworkManager postURI:@""
                 parameters:postParam
                    success:^(id data) {
                        
                        [SVProgressHUD dismiss];
                        BOOL status=data[@"flag"];
                        
                        if (status) {
                            NSArray *programsArray = [[NSArray alloc]init];
                            programsArray = [Episode  mapEpisodFromArray:data[@"episodes"]] ;
                            
                            success (programsArray);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) submitCompetitionWithCompetitionID:(NSString *)cId
                      Answer:(NSString *)answer
                        FullName:(NSString *)fullName
                        Email:(NSString *)email
                        Phone:(NSString *)phone
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure{
    
    
    
    NSDictionary * postParam = @{
                                 @"competition_id":cId,
                                 @"answer":answer,
                                 @"full_name":fullName,
                                 @"email":email,
                                 @"phone":phone,
                                 @"request":URL_SUBMIT_COMPETITION
                                 
                                 };
    
    [NetworkManager postURI:@""
                 parameters:postParam
                    success:^(id data) {
                        
                        [SVProgressHUD dismiss];
                        BOOL status=data[@"flag"];
                        
                        if (status) {
                     
                            success (data);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) getCompetitionSuccess:(serviceSuccess)success
                         failure:(serviceFailure)failure{
    
    
    
    NSDictionary * postParam = @{
                                
                                 @"request":URL_GET_COMPETITION
                                 
                                 };
    
    [NetworkManager postURI:@""
                 parameters:postParam
                    success:^(id data) {
                        
                        [SVProgressHUD dismiss];
                        BOOL status=data[@"flag"];
                        
                        if (status) {
                             [DataManager sharedManager].competition = [[Competition alloc]initWithDictionary:data[@"competition"]];
                            
                            
                        }
                        
                        success(data);
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}

+(void)getNewsSuccess:(serviceSuccess)success failure:(serviceFailure)failure{
    
    NSDictionary * postParam = @{
                                 @"request":URL_GET_NEWS};
    
    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        
        success(data);
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}

+(void)getNewsWithCategoryID:(NSString *)catID
                      Pageno:(NSString *)pageno
                    PageSize:(NSString *)pagesize
                     success:(serviceSuccess)success
                     failure:(serviceFailure)failure{
    NSDictionary * postParam = @{
                                 @"category_id":catID,
                                 @"page_no":pageno,
                                 @"page_size":pagesize,
                                 @"request":URL_GET_NEWS
                                 
                                 };
    [NetworkManager postURI:@"" parameters:postParam success:^(id data) {
        [SVProgressHUD dismiss];
         success(data);
    } failure:^(NSError *error) {
        
    }];
    

}
@end
