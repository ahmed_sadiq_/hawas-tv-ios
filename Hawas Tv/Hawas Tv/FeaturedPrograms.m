//
//  FeaturedPrograms.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "FeaturedPrograms.h"

@implementation FeaturedPrograms

-(id)initWithDictionary:(NSDictionary *)responseData{
    self = [super init];
    if(self){
        self.programTitle = [self validStringForObject:responseData[@"vod_program_title"]];
        self.programTitle = [self stringByStrippingUnicode:self.programTitle];
        self.programId = [self validStringForObject:responseData[@"vod_program_id"]];
        self.programDescription = [self validStringForObject:responseData[@"vod_program_description"]];
        self.programDescription = [self stringByStrippingHTML:self.programDescription];
        self.programImg = [self validStringForObject:responseData[@"vod_program_image"]];
        self.programFolderId = [self validStringForObject:responseData[@"vod_program_episodes_folder_id"]];
        self.programTiming = responseData[@"vod_program_timing"];
    }
    return self;

}

+ (NSArray *)mapProgramFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[FeaturedPrograms alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

- (NSString *)stringByStrippingHTML : (NSString *)str {
    NSRange r;
    NSString *s = str ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    NSString *texttoDisplay = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    texttoDisplay = [texttoDisplay stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    return texttoDisplay;
}

-(NSString *)stringByStrippingUnicode: (NSString *)str{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    return attrStr.string;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.programId forKey:@"programId"];
    [encoder encodeObject:self.programTitle forKey:@"programTitle"];
    [encoder encodeObject:self.programImg forKey:@"programImg"];
    [encoder encodeObject:self.programDescription forKey:@"programDescription"];
    [encoder encodeObject:self.programTiming forKey:@"programTiming"];
    [encoder encodeObject:self.programFolderId forKey:@"programFolderId"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.programId = [decoder decodeObjectForKey:@"programId"];
        self.programTitle = [decoder decodeObjectForKey:@"programTitle"];
        self.programImg = [decoder decodeObjectForKey:@"programImg"];
        self.programDescription = [decoder decodeObjectForKey:@"programDescription"];
        self.programTiming = [decoder decodeObjectForKey:@"programTiming"];
        self.programFolderId = [decoder decodeObjectForKey:@"programFolderId"];
    }
    return self;
}

@end
