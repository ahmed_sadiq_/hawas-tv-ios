//
//  CompetitionVc.h
//  Hawas Tv
//
//  Created by Samreen on 22/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "Competition.h"

@interface CompetitionVc : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *compPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblGiftName;

@end
