//
//  SigninViewController.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 05/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SigninViewController : UIViewController
- (IBAction)btnforgotPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;

- (IBAction)btnSignupPressed:(UIButton *)sender;
- (IBAction)btnSigninPressed:(UIButton *)sender;
@end
