//
//  LatestNews.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "LatestNews.h"

@implementation LatestNews
-(id)initWithDictionary:(NSDictionary *)responseData{
    self = [super init];
    if(self){
        self.newsID = [self validStringForObject:responseData[@"news_id"]];
        self.newsCatName = [self validStringForObject:responseData[@"category_name"]];
        self.newsTitle = [self stringByStrippingHTML:responseData[@"news_title"]];
        self.newsTitle = [self stringByStrippingUnicode:self.newsTitle];
        self.newsDescription = [self stringByStrippingHTML:responseData[@"news_description"]];
        self.newsImage = [self validStringForObject:responseData[@"news_image"]];
    }
    return  self;
}

+(NSArray *)mapNewsFromArray:(NSArray *)arrlist{

    NSMutableArray * mappedArr = [NSMutableArray new];
    for (NSDictionary *dict in arrlist) {
        [mappedArr addObject:[[LatestNews alloc] initWithDictionary:dict]];
    }
    return mappedArr;
}

-(NSString *)stringByStrippingUnicode: (NSString *)str{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    return attrStr.string;
}

- (NSString *)stringByStrippingHTML : (NSString *)str {   
    NSRange r;
    NSString *s = str ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    NSString *texttoDisplay = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    texttoDisplay = [texttoDisplay stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    return texttoDisplay;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.newsID forKey:@"newsID"];
    [encoder encodeObject:self.newsCatName forKey:@"newsCatName"];
    [encoder encodeObject:self.newsTitle forKey:@"newsTitle"];
    [encoder encodeObject:self.newsDescription forKey:@"newsDescription"];
    [encoder encodeObject:self.newsImage forKey:@"newsImage"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.newsID = [decoder decodeObjectForKey:@"newsID"];
        self.newsCatName = [decoder decodeObjectForKey:@"newsCatName"];
        self.newsTitle = [decoder decodeObjectForKey:@"newsTitle"];
        self.newsDescription = [decoder decodeObjectForKey:@"newsDescription"];
        self.newsImage = [decoder decodeObjectForKey:@"newsImage"];
    }
    return self;
}
@end
