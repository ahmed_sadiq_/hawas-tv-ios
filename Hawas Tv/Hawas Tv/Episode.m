//
//  Episode.m
//  Hawas Tv
//
//  Created by Samreen on 19/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "Episode.h"

@implementation Episode
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.episode_id = [self validStringForObject:responseData[@"episode_id"]];
        self.episode_title = [self validStringForObject:responseData[@"episode_title"]];
        self.episode_title = [self stringByStrippingUnicode:self.episode_title];
        self.episode_thumbnail = [self validStringForObject:responseData[@"episode_thumbnail"]];
        self.episode_video_url = [self validStringForObject:responseData[@"episode_video_url"]];
        self.episode_uploaded_date = [self validStringForObject:responseData[@"episode_uploaded_date"]];
        self.episode_modification_date = responseData[@"episode_modification_date"];
        
        
    }
    
    return self;
}




+ (NSArray *)mapEpisodFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Episode alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

-(NSString *)stringByStrippingUnicode: (NSString *)str{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    return attrStr.string;
}

@end
