//
//  News.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 09/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "News.h"
#import "LatestNews.h"

@implementation News
-(id)initWithDictionary:(NSDictionary *)responseData{
    self = [super init];
    if(self){
        self.categoryID = [self validStringForObject:responseData[@"category_id"]];
        self.categoryName = [self validStringForObject:responseData[@"category_name"]];
        self.newsArray = [LatestNews mapNewsFromArray:responseData[@"news"]];
    }
    return  self;
}

+(NSArray *)mapNewsFromArray:(NSArray *)arrlist{
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    for (NSDictionary *dict in arrlist) {
        [mappedArr addObject:[[News alloc] initWithDictionary:dict]];
    }
    return mappedArr;
}

- (NSString *)stringByStrippingHTML : (NSString *)str {
    NSRange r;
    NSString *s = str ;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    NSString *texttoDisplay = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    texttoDisplay = [texttoDisplay stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    return texttoDisplay;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.categoryID forKey:@"category_id"];
    [encoder encodeObject:self.categoryName forKey:@"category_name"];
    [encoder encodeObject:self.newsArray forKey:@"news"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.categoryID = [decoder decodeObjectForKey:@"category_id"];
        self.categoryName = [decoder decodeObjectForKey:@"category_name"];
        self.newsArray = [decoder decodeObjectForKey:@"news"];
    }
    return self;
}
@end
