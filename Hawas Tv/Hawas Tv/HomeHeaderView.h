//
//  HomeHeaderView.h
//  Hawas Tv
//
//  Created by Samreen on 18/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Programs.h"
#import "FeaturedPrograms.h"
#import "LatestNews.h"
@class HomeHeaderView;
@protocol ProgramHeaderViewDelegate <NSObject>
-(void) didPressProgramDetailedButton:(NSInteger )tag;
@end

@interface HomeHeaderView : UITableViewCell <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@property (assign, nonatomic) BOOL isOverlay;
@property (assign, nonatomic) BOOL isFeatured;
@property (assign, nonatomic) BOOL isLatesNews;
@property (assign, nonatomic) BOOL isShadowOverlay;
@property (strong, nonatomic) Programs *prog;
@property (strong, nonatomic) FeaturedPrograms *fProg;
@property (strong, nonatomic) LatestNews *latestNews;
@property (assign, nonatomic) id<ProgramHeaderViewDelegate> headerDelegate;

-(void) setUpScrollView:(NSArray *)programsArray;


@end
