//
//  NewsDetailViewController.h
//  Hawas TV
//
//  Created by Ahmed Sadiq on 06/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "ViewController.h"
#import "Programs.h"
#import "LatestNews.h"

@interface NewsDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *newsDescription;
@property (strong, nonatomic) LatestNews *lNews;


@end
