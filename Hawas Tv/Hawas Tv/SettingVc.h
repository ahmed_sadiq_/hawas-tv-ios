//
//  SettingVc.h
//  Hawas Tv
//
//  Created by Samreen on 24/05/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingVc : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnSignout;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UIView *usernameLine;
@property (weak, nonatomic) IBOutlet UIView *editLine;
@property (weak, nonatomic) IBOutlet UISwitch *pushSwitch;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblPushNoti;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topPushConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topPushBtnConstraint;

@property (assign, nonatomic) BOOL isSignin;
@end
