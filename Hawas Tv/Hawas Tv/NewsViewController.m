//
//  NewsViewController.m
//  Hawas TV
//
//  Created by Ahmed Sadiq on 06/10/2017.
//  Copyright © 2017 Samreen. All rights reserved.
//

#import "NewsViewController.h"
#import "HomeHeaderView.h"
#import "Programs.h"
#import "HomeCell.h"
#import "UIImageView+URL.h"
#import "PostService.h"
#import "HomeFooterView.h"
#import "CategoryCell.h"
#import <HMSegmentedControl.h>
#import "NewsDetailViewController.h"
#import "News.h"
#import "LatestNews.h"
#import "Reachability.h"

@interface NewsViewController ()<ProgramHeaderViewDelegate>
{
    NSMutableArray *programsAarray;
    int pageNo;
    BOOL dataArrayISEmpty;
    BOOL serverCall;
    News *news;
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initialization{
    _firstLoad = YES;
    _isFirstTime = YES;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *savedata = [defaults objectForKey:@"news"];
    _selectedNewsArray = [NSKeyedUnarchiver unarchiveObjectWithData:savedata];
    NSData *newsdata = [defaults objectForKey:@"newsArray"];
    _newsArray = [NSKeyedUnarchiver unarchiveObjectWithData:newsdata];
    
    _segmentArray = [defaults objectForKey:@"segmentArray"];
    [defaults synchronize];
    
    programsAarray = [[NSMutableArray alloc]init];
    [self setupRefreshControl];
    
    _check = true;
    pageNo = 1;
    dataArrayISEmpty =NO;
    serverCall = NO;
    
    NSInteger showCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"showCount"];
    if(showCount > 1 && showCount <= 2){
         // not first time
        _tblView.hidden = NO;
    }
    else{
         //first time
        if(![self internetAvailable]) {
            _tblView.hidden = YES;
            [self showAlert:@"يرجى التحقق من الاتصال بالإنترنت!"];
        } else {
            //internet connection
            _tblView.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"showCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
   
    
    [self getPrograms];

}

#pragma mark - HeaderView Delegate

-(void)didPressProgramDetailedButton:(NSInteger )tag{
    _headerNews = [DataManager sharedManager].latestNews;
    LatestNews *lNews = _headerNews[tag];
    [self performSegueWithIdentifier:@"newsdetailvc" sender:lNews];
}

#pragma mark - TableView Delegates

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *HeaderCellIdentifier = @"header";
    HomeHeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
    
    
    if (headerView == nil) {
        headerView = [[HomeHeaderView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
    }
    headerView.headerDelegate = self;
    headerView.isShadowOverlay = YES;
    headerView.isFeatured = NO;
    headerView.isLatesNews = YES;
    _headerNews = [DataManager sharedManager].latestNews;
    [headerView setUpScrollView:_headerNews.mutableCopy];
    
    return headerView;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 294;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *latestNews = [DataManager sharedManager].latestNews;
    for (int i = 0; i <latestNews.count; i++) {
        LatestNews *fProg = latestNews[i];
        for (int j = 0; j<_selectedNewsArray.count; j++) {
            LatestNews *prog = _selectedNewsArray[j];
            if(fProg.newsID == prog.newsID){
                [_selectedNewsArray removeObjectAtIndex:j];
            }
        }
    }
    return _selectedNewsArray.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if(indexPath.row == 0){
        CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categorycell" forIndexPath:indexPath];
        
        if (cell==nil) {
            
            cell =[[CategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"categorycell"];
            
            
        }
        cell.cellDelegate = self;
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (int i = 0; i < _newsArray.count; i++) {
            news = [_newsArray objectAtIndex:i];
            NSString *catName = news.categoryName;
            [array addObject:catName];
        }
        if(!_isFirstTime){
            [cell setUpCategory:array withIndex:_selectedIndex withWidth:[[UIScreen mainScreen] bounds].size.width];
        }
        else if(_firstLoad){
            [cell setUpCategory:_segmentArray withIndex:_segmentArray.count - 1 withWidth:[[UIScreen mainScreen] bounds].size.width];
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:array forKey:@"segmentArray"];
      
        return cell;
    }
    else{
        LatestNews *prog = _selectedNewsArray[indexPath.row - 1];
        HomeCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        if (cell==nil) {
            
            cell =[[HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            
        }
        [cell.postImg setImagewithURL: prog.newsImage];
        cell.lblCategory.text = prog.newsCatName;
        cell.lblTitle.text =prog.newsTitle;

        
        NSLog(@"cellsize %f",cell.lblTitle.frame.size.height);
        
        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 65;
    }
    else
        return 280;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LatestNews *lNews = _selectedNewsArray[indexPath.row-1];
    
    [self performSegueWithIdentifier:@"newsdetailvc" sender:lNews];
}

#pragma mark - CategoryCell Delegate
-(void)didTapCategoryWithIndex:(NSInteger)selectedIndex{
    _selectedIndex = selectedIndex;
    pageNo = 1;
    if(_check){
        [self getCategoryNewsWithID:selectedIndex];
    }
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSArray *visibleRows = [_tblView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_tblView indexPathForCell:lastVisibleCell];
    NSLog(@"%ld",(long)path.row);
    if(path.section == 0 && path.row == (_selectedNewsArray.count))
    {
        if ( !serverCall && !dataArrayISEmpty) {
            pageNo++;
            [self getCategoryNewsWithID:_selectedIndex];
        }
    }
}


-(void) getPrograms{
    
    serverCall = YES;
    
    [PostService getNewsSuccess:^(id data) {
        [DataManager sharedManager].news = [News mapNewsFromArray:data[@"categories"]];
        _newsArray = [DataManager sharedManager].news;
        
        NSData *newsdata = [NSKeyedArchiver archivedDataWithRootObject:_newsArray];
        [[NSUserDefaults standardUserDefaults] setObject:newsdata forKey:@"newsArray"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        [self getCategoryNewsWithID:_newsArray.count-1];
        
    } failure:^(NSError *error) {
        NSLog(@"error in news: %@",error.localizedDescription);
    }];
}

-(void) getCategoryNewsWithID:(long)catID{
    _check = false;
    serverCall = YES;
    NSString *page = [NSString stringWithFormat:@"%d", pageNo];
    News *name = [_newsArray objectAtIndex:catID];
    [PostService getNewsWithCategoryID:name.categoryID Pageno:page PageSize:@"10" success:^(id data) {
        _tblView.hidden = NO;
        _check = true;
        [self.refreshControl endRefreshing];
        NSArray *tempPost = data;
        NSArray *currentNewsArray = [News mapNewsFromArray:data[@"categories"]];
        News *currentNews = [currentNewsArray objectAtIndex:0];
        if(_firstLoad){
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *featureddata = [NSKeyedArchiver archivedDataWithRootObject:currentNews.newsArray];
            [defaults setObject:featureddata forKey:@"news"];
            _firstLoad = NO;
        }
        if (tempPost.count == 0) {
            dataArrayISEmpty = YES;
        }
        if (pageNo==1) {
            [_selectedNewsArray removeAllObjects];
            _selectedIndex = catID;
            _selectedNewsArray = currentNews.newsArray.mutableCopy;
            [_tblView reloadData];
            serverCall = NO;
            dataArrayISEmpty = NO;
            _isFirstTime = NO;
        }
        else{
            _count = _selectedNewsArray.count;
            for (LatestNews *place in currentNews.newsArray) {
                [_selectedNewsArray addObject:place];
            }
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            int startIndex = (pageNo-1) * 10;
            for (NSInteger i = _count ; i < _count+10; i++) {
                if(i<_selectedNewsArray.count) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
            }
            [_tblView beginUpdates];
            [_tblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            [_tblView endUpdates];
            serverCall = NO;
            
            
        }
        NSLog(@"%@",data);
    } failure:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
    }];

}

- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}
- (void)refreshHome:(id)sender
{
    pageNo=1;
    if (!serverCall) {
        
        [self getPrograms];
        
    }
}

#pragma mark - InternetConnection Method

- (BOOL)internetAvailable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    } else {
        return true;
    }
}

#pragma mark - Alert Method
-(void)showAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"خطئ" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"إعادة المحاولة" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(![self internetAvailable]) {
            [self showAlert:@"يرجى التحقق من الاتصال بالإنترنت!"];
        } else {
            _tblView.hidden = YES;
            [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"showCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self getPrograms];
        }
    }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated: YES completion: nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"newsdetailvc"]){
        NewsDetailViewController *vc = segue.destinationViewController;
        vc.lNews = sender;
    }
}


@end
